import java.util.*;
import Constructor.Person;
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        ArrayList<Person> arrayList = new ArrayList<>();

        //khoi tao cac person voi cac tham so khac nhau
        Person person0= new Person();
        Person person1= new Person("Devcamp");
        Person person2= new Person("Viet", 24, 66.8);
        Person person3= new Person("Nam", 30, 68.8, 50000000, new String[] {"Little Camel"});
    
        arrayList.add(person0);
        arrayList.add(person1);
        arrayList.add(person2);
        arrayList.add(person3);
        //in ra
        for (Person person:arrayList){
            System.out.println(person);
        }
    
    }
}
