
package Constructor;

import java.io.*;
import java.util.Arrays;

public class Person {

    String name;
    int age;

    double weight;
    long salary;
    String[] pets;
    
    public Person (String name){
        this.name= name;
        this.age= 18;
        this.weight= 60.5;
        this.salary=10000000;
        this.pets= new String[] {"Big Dog", "Small Cat", "Gold Fish", "Red Parriot"};
    }
//tat ca cac tham so
    public Person (String name, int age, double weight,long salary, String[] pets){
        this.name= name;
        this.age= 18;
        this.weight= 60.5;
        this.salary=10000000;
        this.pets= pets;
    }
//khoong tham public static void name() {
    public Person (){
        this("TuiDuocHon");
    }

// 3 tham so
    public Person (String name, int age, double weight){
        this(name, age, weight, 20000000, new String[] {"Big Dog", "Small Cat", "Gold Fish", "Red Parriot"});
}

    public String toString(){
        return "Person [name= "+name+", age= "+age+" , weight="+ weight+ " , salary= "+salary
        + " , pets= "+ Arrays.toString(pets)+"]";
    }
}
   

    